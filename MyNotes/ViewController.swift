//
//  ViewController.swift
//  MyNotes
//
//  Created by Private on 2/4/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var table: UITableView!
    
    var myData: [String] = []
    var selectedRow: Int = -1
    var newRowText:String = ""
    var detailView: DetailViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My Notes"
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(AddNewNotes))
        self.navigationItem.rightBarButtonItem = addButton
        self.navigationItem.leftBarButtonItem = editButtonItem
        load()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if selectedRow == -1 {
            return
        }
        myData[selectedRow] = newRowText
        if newRowText == "" {
            myData.remove(at: selectedRow)
        }
        table.reloadData()
        save()
    }
    
    @objc func AddNewNotes(){
        if table.isEditing{
            return
        }
        
        let name:String = ""
        myData.insert(name, at: 0)
        let indexPath: IndexPath = IndexPath(row: 0, section: 0)
        table.insertRows(at: [indexPath], with: .automatic)
        table.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        self.performSegue(withIdentifier: "detail", sender: nil )
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = myData[indexPath.row]
        return cell
        
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        table.setEditing(editing, animated: animated)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        myData.remove(at: indexPath.row)
        table.deleteRows(at: [indexPath], with: .fade)
        
        save()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "detail", sender: nil )
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailView:DetailViewController = segue.destination as!DetailViewController
        selectedRow = table.indexPathForSelectedRow!.row
        detailView.masterView = self
        detailView.setText(t: myData[selectedRow])
    }
    
    
    func save() {
        UserDefaults.standard.set(myData, forKey: "notes")
        UserDefaults.standard.synchronize()
    }
    
    func load(){
        if let loadData = UserDefaults.standard.value(forKey: "notes") as? [String] {
            myData = loadData
            table.reloadData()
            
        }
        
    }
}
