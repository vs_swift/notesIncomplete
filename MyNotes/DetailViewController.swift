 //
//  DetailViewController.swift
//  MyNotes
//
//  Created by Private on 2/4/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit
import CoreLocation

class DetailViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var viewText: UITextView!
    var text:String = ""
    var masterView: ViewController!
    
    // Location
    var locationManager = CLLocationManager()
    var location: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewText.text = text
        self.title = "notes"
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func setText(t:String){
        text = t
        if isViewLoaded{
            viewText.text = t
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        masterView.newRowText = viewText.text
    }
    
    @IBAction func pressRefresh(_ sender: Any) {
        refreshLocation()
    }
    
    func refreshLocation() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error)->Void in
                if placemarks![0].locality != nil {
                    self.location += String(describing: placemarks![0].locality!) + ", "
                }
                if placemarks![0].administrativeArea != nil {
                    self.location += String(describing: placemarks![0].administrativeArea!) + " "
                }
                if placemarks![0].postalCode != nil {
                    self.location += String(describing: placemarks![0].postalCode!) + ", "
                }
                if placemarks![0].country != nil {
                    self.location += String(describing: placemarks![0].country!)
                }
                self.locationLabel.text = self.location
                self.location = ""
            })
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed")
    }
    
 }
